<?php
/**
 * Implements hook_rules_condition_info().
 *
 * Adds new rule conditions to commerce_line_item entity type.
 */
function commerce_combo_discount_rules_condition_info() {
  return array(
    'combo_discount' => array(
      'label' => t('Apply combo discount to products'),
      'parameter' => array(
        'commerce_line_item' => array(
          'type' => 'commerce_line_item',
          'label' => t('Line Item'),
          'description' => t('A product line item.'),
          'wrapped' => TRUE,
        ),
        'products' => array(
          'type' => 'text',
          'label' => t('Product SKU(s)'),
          'description' => t('Products SKU to look for on the order. Enter a comma-separated list of product SKU(s).'),
        ),
        'quantity' => array(
          'type' => 'integer',
          'label' => t('Quantity'),
          'description' => t('Quantity value to be compared against each selected product(s).'),
        ),
      ),
      'group' => t('Commerce Order'),
      'callbacks' => array(
        'execute' => 'commerce_combo_discount_build',
      ),
    )
  );
}

/**
 * Build callback for commerce_combo_discount.
 *
 * This condition is evaluated for every line item in the order, so we use a
 * static variable to check if the discount is valid for this line item
 * considering also the rest of the order.
 *
 * @param EntityDrupalWrapper $wrapper
 *   The wrapped entity given by the rule.
 * @param string $searched_product_sku
 *   The SKU of the product searched in line_item order.
 * @param int $quantity
 *   Product quantity multiplier.
 *
 * @return bool
 *   True if the condition is valid. False otherwise.
 */
function commerce_combo_discount_build(EntityDrupalWrapper $wrapper, $searched_product_sku, $quantity) {
  $static_products = &drupal_static(__FUNCTION__, array());
  $return = FALSE;
  $discounted_product_sku = $wrapper->commerce_product->sku->value();
  $order_wrapper = $wrapper->order;

  if (empty($static_products)) {
    foreach ($order_wrapper->commerce_line_items as $wrapper_line_item) {
      if (in_array('commerce_product', array_keys($wrapper_line_item->getPropertyInfo()))) {
        if (empty($static_products[$wrapper_line_item->commerce_product->sku->value()])) {
          $static_products[$wrapper_line_item->commerce_product->sku->value()] = intval($wrapper_line_item->quantity->value());
        }
        else {
          $static_products[$wrapper_line_item->commerce_product->sku->value()] += intval($wrapper_line_item->quantity->value());
        }
      }
    }
  }

  if (!empty($static_products[$searched_product_sku]) && !empty($static_products[$discounted_product_sku])) {
    $return = ($static_products[$searched_product_sku] >= (1 * $quantity));
    $static_products[$searched_product_sku] = - 1 * $quantity;
    $static_products[$discounted_product_sku]--;
  }

  return $return;
}
